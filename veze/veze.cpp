#include <iostream>
#include <assert.h>
using namespace std;
const int N = 5;

struct Vez
{
    int v;
    int p[N];
};

void print();

void init(Vez& X) {
    X.v = 0;
    for (int i = 0; i < N; i++) {
        X.p[i] = 0;
    }
}

void setup(Vez& X) {
    X.v = N;
    for (int i = 0; i < N; i++) {
        X.p[i] = N-i;
    }
}

void krok (Vez& X, Vez& Y) {
    assert(X.v > 0);
    int d = X.p[X.v - 1];
    X.p[X.v - 1] = 0;
    X.v --;

    assert (Y.v < N);
    if (Y.v > 0)
        assert (Y.p[Y.v - 1] > d);
    
    Y.v ++;
    Y.p[Y.v - 1] = d;
}

void hraj(Vez& X, Vez& Y, Vez& Z, int k) {
    if (k > 1) { hraj(X, Z, Y, k - 1); }
    krok(X, Z);
    print();
    if (k > 1) { hraj(Y, X, Z, k - 1);  }

}

 void print (Vez& X) {
    for(int i=0;i<N;i++)
        if (i < X.v) {
            cout << X.p[i] << " ";
        }
        else {
            cout << "  ";
        }
}

Vez A, B, C;

void print() {
    print(A);
    cout << " : ";
    print(B);
    cout << " : ";
    print(C);
    cout <<endl;
}

int main()
{
    setup(A);
    init(B);
    init(C);

    print();
    hraj(A, B, C, N);
    print();


    /*
    krok(A, B);
    print();

    krok(A, C);
    print();

    krok(B, C);
    print();
    */

    cout << "O.K." << endl;
}
